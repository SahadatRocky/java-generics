package com.example.java.generics;

import java.util.ArrayList;
import java.util.List;

public class Example {
    public static void main(String[] args){

        List<Integer> numbers = new ArrayList<>();
        numbers.add(1);
        numbers.add(2);

        // approch-1
        for(Integer number : numbers){
            System.out.println("d:" + number);
        }

        // approch-2
//        numbers.forEach(data ->{
//            System.out.println("data:"+ data);
//        });
    }
}
