package com.example.java.generics;

public class GenericAndMethods {

    public static void main(String[] args){

        String ali = "ali";
        String[] names = {ali,"mohammad"};
        Character[] letters = {'A','B','C','D'};
        Integer[] numbers = {1,2,3,4,5,6};

        print(names);
        print(letters);
        print(numbers);

    }

    static <T> void print(T[] array){
        for(T item: array){
            System.out.println(item.getClass().getName()+"-"+ item);
        }
    }

}
