package examples;

import java.util.*;

public class GenericType {
    public static void main(String[] args) {
        List<String> names = new ArrayList<String>();
        names.add("joss");
        names.add("lol");
        names.add("cds");
        names.add("sd");

        List<Double> doubles = new ArrayList<Double>();
        doubles.add(1.3);
        doubles.add(4.33);
        doubles.add(8.34);
        doubles.add(2.76);

        names.forEach(s -> {
            System.out.println(s);
        });

        doubles.forEach(d->{
            System.out.println(d);
        });

        Map<List<?>,List<?>> map1 = new HashMap<>();
        map1.put(names,doubles);

        map1.forEach( (s, d) -> {
            System.out.println(s + "-" + d);
        });

        Map<String, Double> map2 = new HashMap<>();
        map2.put("ki", 2.33);

        map2.forEach( (s, d) -> {
            System.out.println(s + "-" + d);
        });

        Map<Number, Double> map3 = new HashMap<>();
        map3.put(1, 55.6);

        map3.forEach( (s, d) -> {
            System.out.println(s + "-" + d);
        });

        Map<Integer, String> map4 = new HashMap<>();
        map4.put(4, "nice");

        map4.forEach( (s, d) -> {
            System.out.println(s + "-" + d);
        });


    }
}
