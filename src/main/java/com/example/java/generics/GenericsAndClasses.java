package com.example.java.generics;

import com.example.java.generics.model.Box;
import com.example.java.generics.model.Letter;
import com.example.java.generics.model.Phone;

public class GenericsAndClasses {

    public static void main(String[] args){
         Box<Letter> b = new Box<Letter>();
         b.setT(new Letter("Amigosco"));
        System.out.println(b.getT());

         Box<Phone> p = new Box<Phone>();
         p.setT(new Phone("Apple12"));
        System.out.println(p.getT());

    }
}
