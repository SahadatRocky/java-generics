package examples;

import java.util.Arrays;
import java.util.List;

public class LowerBoundedWildcards {

    public static void main(String[] args) {

        // ? wildcards
        // LowerBoundedWildcards
        List<Integer> list1 = Arrays.asList(1,2);
        List<Number> list2 = Arrays.asList();

        print(list1);
        print(list2);


    }

    static <T> void print(List<? super T> list){
        list.forEach( l -> {
            System.out.println(l);
        });
    }


}
