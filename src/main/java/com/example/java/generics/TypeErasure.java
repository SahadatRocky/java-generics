package examples;
public class TypeErasure {
    public static void main(String[] args) {
        Node<Phone> n = new Node<Phone>(new Phone("Huawei"), null);
        Node<Phone> n1 = new Node<Phone>(new Phone("samsung"), n);

//      System.out.println(n.getData());
        System.out.println(n1.getData());
        System.out.println(n1.getNext().getData());

        Phone p  = new Phone("samsung");
        System.out.println(p.getBrand());

    }
}
