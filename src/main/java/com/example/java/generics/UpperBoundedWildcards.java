package examples;

import java.util.Arrays;
import java.util.List;

public class UpperBoundedWildcards {
    public static void main(String[] args) {
        // ? Wildcards
        // Upper Bounded Wildcards
        List<Integer> list1 = Arrays.asList(1,4);
        List<Double> list2 = Arrays.asList(1.2,4.3);
        List<String> list4 = Arrays.asList("hi", "sweetheart");
        List<Number> list3 = Arrays.asList();

        print(list1);
        print(list2);
        print(list3);
        print(list4);

    }

    static <T> void print(List<? extends T> list){
          list.forEach( l -> {
              System.out.println(l.getClass().getName());
              System.out.println(l);
          });
    }
}
