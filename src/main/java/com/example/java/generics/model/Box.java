package com.example.java.generics.model;

public class Box<T>{
    private T t;

    public void setT(T t){
        this.t = t;
    }

    public T getT(){
        return this.t;
    }

}
