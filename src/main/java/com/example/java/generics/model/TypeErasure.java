package com.example.java.generics.model;

public class TypeErasure {

    public class Node<T>{
        private Object data;
        private Node<T> next;

        public Node(Object data, Node<T> next) {
            this.data = data;
            this.next = next;
        }

        public Object getData() {
            return data;
        }

        public Node<T> getNext() {
            return next;
        }
    }

    /*
    public class Node{
         private Object data;
         private Node next;

         public Node(Object data, Node next){
             this.data = data;
             this.next = next;
         }

         public Object getData(){
             return this.data;
         }

    }
    */

}
